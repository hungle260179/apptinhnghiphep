export default {
   methods: {
      /**
       * check validate required
       * @param value - Giá trị cần kiểm tra
       * @returns - True nếu giá trị hợp lệ, chuỗi lỗi nếu giá trị không hợp lệ
       */
      validateInput(value: string): boolean | string {
         if (!value) {
            return 'This field is required';
         }
         return true;
      },

      /***
       * Validate email
       * @param value - Giá trị cần kiểm tra
       * @returns - True nếu giá trị hợp lệ, chuỗi lỗi nếu giá trị không hợp lệ
       */
      validateEmail(value: string): boolean | string {
         // if the field is empty
         if (!value) {
            return 'This field is required';
         }
         // if the field is not a valid email
         const regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
         if (!regex.test(value)) {
            return 'This field must be a valid email';
         }
         // All is good
         return true;
      },

      /**
       * check confirm password
       * @param value - Giá trị cần kiểm tra
       * @returns - True nếu giá trị hợp lệ, chuỗi lỗi nếu giá trị không hợp lệ
       */
      validateConfirmPassword(value: string): boolean | string {
         if (!value) {
            return 'This field is required';
         }
         return true;
      }
   }
};