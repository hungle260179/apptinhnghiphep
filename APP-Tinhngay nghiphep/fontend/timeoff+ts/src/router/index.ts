
import { vue } from '@vitejs/plugin-vue';
import Login from '../views/pages/Account/Login.vue';
import Register from '../views/pages/Account/Register.vue';
import { createRouter, createWebHistory } from 'vue-router'
import CreateApplication  from '../views/pages/Application/new.vue'
import Dashboard from '../layouts/Dashboard.vue'
import UserManage from '../views/pages/Account/manage.vue'
import AdminRoles from '../components/Admin/AdminRoles.vue'
import InfoAccount from "../views/pages/Account/info.vue";
import ListApplicationUser from "../views/pages/Account/list.vue";
import ListApplication from '../views/pages/Application/index.vue';
import EditApplication from '../views/pages/Application/edit.vue';

const isAuthenticated = window.localStorage.getItem('token')

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/register',
      name: 'register',
      component: !isAuthenticated ? Register : Dashboard
  },
  {
      path: '/login',
      name: 'login',
      component: !isAuthenticated ? Login : Dashboard
  },
  {
      path: '/',
      name: 'dashboard',
      component: isAuthenticated ? Dashboard : Login
  },

  {
      path: '/create-application',
      name: 'create-application',
      component: isAuthenticated ? CreateApplication :Login
  },
  {
      path: '/edit-application/:id',
      name: 'edit-application',
      component: isAuthenticated ? EditApplication :Login
  },
  {
      path: '/list',
      name: 'list',
      component: isAuthenticated ? ListApplication : Login
  },
  {
      path: '/manage',
      name: 'manage-user',
      component: isAuthenticated ? UserManage : Login
  },
  {
      path: '/manage/:id',
      name: 'manage-user-role',
      component: isAuthenticated ? AdminRoles : Login
  },
  {
      path: '/info',
      name: 'account-info',
      component: isAuthenticated ? InfoAccount : Login
  },
  {
      path: '/list-appli-user',
      name: 'list-appli-user',
      component: isAuthenticated ? ListApplicationUser : Login
  }
    
  ]
})

export default router
