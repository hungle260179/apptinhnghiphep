interface Leaves{
  id: number;
  type: string;
  start_date: Date | string;
  end_date: Date | string;
  reason: string;
  estimate: number;
  comment: string;
  status: string | number;

}
export default Leaves